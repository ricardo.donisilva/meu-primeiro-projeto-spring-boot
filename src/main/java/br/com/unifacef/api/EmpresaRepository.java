package br.com.unifacef.api;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unifacef.api.entities.Empresa;


public interface EmpresaRepository extends JpaRepository<Empresa, Long>{
// Jparepository esta fazendo que essa classe se comunique com calsse empresa e o chave primaria 
// da classe e tipo Long
	Empresa findByCnpj(String cnpj);
	
}
